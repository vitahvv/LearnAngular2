import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import {RouterModule,Routes} from '@angular/router';
//import { AUTH_PROVIDERS } from './app/auth';

import {AppComponent} from './app.component';
import {ProductsListComponent} from './products-list/products-list.component';
import { ProductRowComponent } from './product-row/product-row.component';
import { ProductImageComponent } from './product-image/product-image.component';
import { PriceDisplayComponent } from './price-display/price-display.component';
import { ProductDepartmentComponent } from './product-department/product-department.component';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { ContactComponent } from './contact/contact.component';
import {APP_BASE_HREF, HashLocationStrategy, LocationStrategy} from "@angular/common";

const routes: Routes = [
  // basic routes
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  { path: 'about', component: AboutComponent },
  { path: 'contact', component: ContactComponent },
  { path: 'contactus', redirectTo: 'contact' }]
//
//   // authentication demo
//   { path: 'login', component: LoginComponent },
//   {
//     path: 'protected',
//     component: ProtectedComponent,
//     canActivate: [ LoggedInGuard ]
//   },
//
//   // nested
//   {
//     path: 'products',
//     component: ProductsComponent,
//     children: childRoutes
//   }
// ];


@NgModule({
  declarations: [
    AppComponent,
    ProductsListComponent,
    ProductRowComponent,
    ProductImageComponent,
    PriceDisplayComponent,
    ProductDepartmentComponent,
    HomeComponent,
    AboutComponent,
    ContactComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(routes)

  ],
  providers: [
     { provide: LocationStrategy, useClass: HashLocationStrategy },
     { provide: APP_BASE_HREF, useValue: '/' } // <--- this right here
  //  AUTH_PROVIDERS
   // LoggedInGuard
  ],
  bootstrap: [AppComponent]

})
export class AppModule {

}
